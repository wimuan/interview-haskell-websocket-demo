module Handlers where

import Control.Concurrent (MVar, readMVar)
import Control.Lens
import Control.Monad.State.Lazy
import Data.Aeson hiding ((.=))
import Data.Maybe (isNothing, fromJust, isJust)
import Data.Text (Text)
import qualified Data.HashMap.Strict as H
import qualified Data.List as L
import qualified Network.WebSockets as WS

import Types
import State

reply :: ToJSON a => a -> App ()
reply msg = do
  conn' <- use conn
  lift $ WS.sendDataMessage conn' frame
  where frame = (`WS.Text` Nothing) . encode . mapObject mapKind . toJSON $ msg

replyInline :: ToJSON a => [(Text, a)] -> App ()
replyInline = reply . Object . H.fromList . map (_2 %~ toJSON)

replyInlineT = replyInline :: [(Text, Text)] -> App ()

-- Authorization

authorized :: App a -> App ()
authorized action = do
  authorized <- isJust <$> use user
  if authorized
  then void action
  else replyInlineT [("$type", "kurvītis"), ("message", "please authorize!")]

authorize :: [User] -> AuthRequest -> Maybe User
authorize users auth =
  if fmap _password user == Just (authPassword auth)
  then user
  else Nothing
  where user = L.find ((== authUsername auth) . _name) users

--

handleRequest :: MVar ServerState -> WS.Connection -> Request -> App ()

-- Basic requests

handleRequest state conn (ReqAuth auth_req) = do
  users <- lift $ (_users <$> readMVar state)

  let user' = authorize users auth_req
  user .= user'

  if isNothing user'
  then replyInlineT [("$type", "login_failed")]
  else replyInlineT
    [ ("$type", "login_successful")
    , ("user_type", _role (fromJust user'))
    ]

handleRequest state conn (ReqPing ping_req) = do
  -- ping also prints all the ids of clients
  lift $ print . ("clients connected: " ++) . show . map (\c -> (_id c, _subscribed c)) . _clients =<< readMVar state

  reply $ ping_req { pingKind = "pong" }

handleRequest _ conn ReqWhoami =
  replyInline . (:[]) . ("user", ) =<< use user

-- Table requests

handleRequest state _conn ReqSubscribe = authorized $ do
  tables <- lift $ view tables <$> readMVar state
  (lift . updateClient state) =<< (subscribed .~ True) <$> get
  replyInline
    [ ("$type", String "table_list")
    , ("tables", toJSON tables)
    ]
