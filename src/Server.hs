module Server where

import Control.Concurrent (MVar)
import Control.Exception (finally)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.State.Lazy
import qualified Network.WebSockets as WS

import Data.Aeson
import Data.ByteString as BS (ByteString)
import Data.Conduit
import Data.Conduit.Attoparsec
import qualified Data.Conduit.List as CL

import Handlers
import State
import Types

dataConduit :: WS.Connection -> Source IO ByteString
dataConduit conn = liftIO (WS.receiveData conn) >>= yield >> dataConduit conn

application :: MVar ServerState -> WS.ServerApp
application state pending = do
  conn <- WS.acceptRequest pending
  WS.forkPingThread conn 30

  client <- addClient state conn

  flip finally (removeClient state client) $
    void $ requestProducer conn $$ flip CL.foldM client $ \client request ->
      snd <$> (flip runStateT client $ handleRequest state conn request)

  -- snd returns state, fst would return return value

  where
    fromJustJSON a =
      case a of
        Success a -> a
        Error a   -> error ("resultToMby: " ++ show a)

    requestProducer :: WS.Connection -> ConduitM () Request IO ()
    requestProducer conn =
      dataConduit conn $=
      conduitParser json $=
      CL.map (fromJustJSON . fromJSON . mapObject mapType . snd)
