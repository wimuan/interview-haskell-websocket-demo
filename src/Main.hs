module Main where

import Control.Concurrent (newMVar)
import Data.Aeson
import Data.Maybe
import qualified Data.ByteString.Lazy.Char8 as BSLC
import qualified Network.WebSockets as WS

import Server
import Types

getUsers :: IO [User]
getUsers = fromJust . decode . BSLC.pack <$> readFile "data/users.json"

main :: IO ()
main = do
    state <- newMVar . ServerState [] [] =<< getUsers
    WS.runServer "127.0.0.1" 9160 $ application state
