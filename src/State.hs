module State where

import Control.Concurrent (MVar, modifyMVar, modifyMVar_)
import Control.Lens
import qualified Network.WebSockets as WS

import Types

addClient :: MVar ServerState -> WS.Connection -> IO Client
addClient state conn = do
  modifyMVar state $ \state -> do
    let id = (+1) . foldl max 0 . map _id  . _clients $ state
    let client = Client id conn Nothing False
    return ((clients %~ (client :)) state, client)

removeClient :: MVar ServerState -> Client -> IO ()
removeClient state client = do
  modifyMVar_ state $ return . (clients %~ filter (\c -> _id c /= _id client))

updateClient :: MVar ServerState -> Client -> IO ()
updateClient state client = do
  modifyMVar_ state $ return .
    (clients %~ (map $ \needle ->
      if _id needle == _id client
      then client
      else needle
    ))
