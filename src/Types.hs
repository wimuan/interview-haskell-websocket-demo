{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TemplateHaskell #-}

module Types where

import Control.Lens
import Control.Monad.State.Lazy
import Data.Aeson
import Data.Aeson.TH
import Data.Char (toLower)
import Data.HashMap.Strict (HashMap, (!))
import Data.Text (Text)
import GHC.Generics (Generic)
import qualified Data.HashMap.Strict as H
import qualified Network.WebSockets as WS

-- can't use "$type" or "type" as they're 1. an illegal identifier, 2. a keyword

data AuthRequest = AuthRequest
  { authKind :: Text
  , authUsername :: Text
  , authPassword :: Text
  } deriving (Show)

deriveJSON defaultOptions
  { fieldLabelModifier = map toLower . drop 4
  , constructorTagModifier  = map toLower
  } ''AuthRequest

data PingRequest = PingRequest
  { pingKind :: Text
  , pingSeq :: Integer
  } deriving (Show)

data Request =
  ReqAuth AuthRequest |
  ReqPing PingRequest |
  ReqWhoami           |
  ReqSubscribe

deriveJSON defaultOptions
  { fieldLabelModifier = map toLower . drop 4
  , constructorTagModifier = id
  } ''PingRequest

renameHash :: Text -> Text -> HashMap Text Value -> HashMap Text Value
renameHash a b h = ($ H.delete a h) $ maybe id id (H.insert b <$> H.lookup a h)

mapType = renameHash "$type" "kind"
mapKind = renameHash "kind" "$type"

mapObject :: (Object -> Object) -> Value -> Value
mapObject f (Object o) = Object (f o)
mapObject _ x        = x

instance FromJSON Request where
  parseJSON (Object v) =
    case v of
      _ | v ! "kind" == "login"            -> ReqAuth <$> parseJSON (Object v)
      _ | v ! "kind" == "ping"             -> ReqPing <$> parseJSON (Object v)
      _ | v ! "kind" == "whoami"           -> return ReqWhoami
      _ | v ! "kind" == "subscribe_tables" -> return ReqSubscribe

---

data Table = Table
  { tableId :: Integer
  , tableName :: Text
  , tableParticipants :: Integer
  } deriving (Show)

deriveJSON defaultOptions
  { fieldLabelModifier = map toLower . drop 5
  , constructorTagModifier  = map toLower
  } ''Table

---

data User = User
  { _name :: Text
  , _password :: Text
  , _role :: Text
  } deriving (Generic, Show)

deriveJSON defaultOptions {fieldLabelModifier = drop 1} ''User

-- The underscore prefixes are there for lenses, by the way.
data Client = Client
  { _id :: Integer
  , _conn :: WS.Connection
  , _user :: Maybe User
  , _subscribed :: Bool
  }

makeLenses ''Client

data ServerState = ServerState
  { _clients :: [Client]
  , _tables :: [Table]
  , _users :: [User]
  }

makeLenses ''ServerState

type App a = StateT Client IO a

-- :set -XDeriveGeneric
-- :l src/Types.hs
-- import qualified Data.ByteString.Lazy.Char8 as BSLC
-- import Data.Maybe
-- fromJust . decode . BSLC.pack <$> readFile "users.json" :: IO (Maybe [User])
