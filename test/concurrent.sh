#!/usr/bin/env bash

convo_concurrent() { (cat "$1"; sleep 5) | ws ws://127.0.0.1:9160/ & sleep 0.5; }
ping() { ws ws://127.0.0.1:9160/ < data/requests/ping.json; }

for json in data/requests/auth-ok-*.json; do
  convo_concurrent "$json"; echo
done

ping

for json in data/requests/auth-ok-*.json; do
  wait
done

sleep 1

ping
