#!/usr/bin/env bash

convo() { (cat "$1"; sleep 1) | ws ws://127.0.0.1:9160/; }

for json in data/requests/*.json; do
  echo "$json"
  convo "$json"; echo
done
